const container = document.querySelector(".container")
const hambs = [
  { name: "Promo", image: "images/promo.jpg" },
  { name: "Fome 1", image: "images/hamb01.jpg" },
  { name: "Fome 2", image: "images/hamb02.jpg" },
]

const showHambs = () => {
  let output = ""
  hambs.forEach(
    ({ name, image }) =>
      (output += `
              <div class="card">
                <img class="card--avatar" src=${image} />
                <h1 class="card--title">${name}</h1>
                <a class="card--link" href="#">Delicia</a>
              </div>
              `)
  )
  container.innerHTML = output
}

document.addEventListener("DOMContentLoaded", showHambs)

if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register("serviceWorker.js")
      .then(res => console.log("sw registrado"))
      .catch(err => console.log("sw não registrado", err))
  })
}
